import path from "path";
import fs from "fs";

/*
 * These constants are kept separate from Constants.ts, since
 * they have server-side imports that webpack might bundle,
 * something that we don't want to happen.
 */

export const RW_ROOT_PATH = (() => {
    const root = path.resolve("/");
    let currentPath = path.resolve(__dirname);

    while (!fs.existsSync(path.join(currentPath, ".redwarn-anchor"))) {
        if (currentPath === root)
            return null;
        else
            currentPath = path.resolve(currentPath, "..");
    }

    return currentPath;
})();