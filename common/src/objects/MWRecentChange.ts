/**
 * Interface dervied from schema at https://www.mediawiki.org/wiki/Manual:RCFeed.
 *
 * Anything that has more information (e.g. user can be an UserInfo) will return
 * its respective interface. If RW is still getting said information, it will be
 * provided as a promise.
 **/
interface MWRecentChange {

    id : string;
    type : string;

    namespace : number;

    title : string;
    comment : string;
    parsedcomment : string;

    timestamp : number; /* API provides a non-Node second-based unix timestamp. */

    user : string;
    bot : boolean;

    /* Common server information. */
    "server_url" : string;
    "server_name"  : string;
    "server_script_path"  : string;
    wiki : string;

    /* If type == "edit" or type == "new", the following will be populated. */
    minor? : boolean;
    patrolled? : boolean;
    length? : {
        old : null | number,
        new : number
    };
    revision? : {
        old: null | number,
        new : number
    };

}

export default MWRecentChange;