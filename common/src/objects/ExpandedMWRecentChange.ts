import MWRecentChange from "./MWRecentChange";
import ORESData from "./ORESData";
import UserData from "./UserData";

/*
 * Decorator adds to the recent change? Put them expansion here.
 *
 * Rules for expansions:
 *  - Make the added variables optional, in case a decorator fails.
 *  - Don't trample over the feet of other Decorators. Be unique with the names.
 */

export interface ORESExpansion { ores?: ORESData | false }
export interface ByteDiffExpansion { bytes?: number }
export interface UserExpansion { userInfo?: UserData }

type ExpandedMWRecentChange = MWRecentChange
    & ByteDiffExpansion
    & ORESExpansion
    & UserExpansion;

export default ExpandedMWRecentChange;