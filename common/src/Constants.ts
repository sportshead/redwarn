/*
RedWarn project constants - change behaviour for your instance here.
*/

import {RW_GIT_HASH} from "./DynamicConstants";

export const RW_DEBUG = process.env.NODE_ENV === "production" ? "production" : "development";

// Version management
export const RW_FRONTEND_VERSION = "0.1.0";
export const RW_BACKEND_VERSION = "0.1.0";

// Version identifiers
export const RW_FRONTEND_IDENTIFIER = `${RW_FRONTEND_VERSION}${
    RW_DEBUG ? "d" : "p"
}-${RW_GIT_HASH.substr(0, 6)}`;
export const RW_BACKEND_IDENTIFIER = `${RW_BACKEND_VERSION}${
    RW_DEBUG ? "d" : "p"
}-${RW_GIT_HASH.substr(0, 6)}`;

/** Backend constants **/

/** Frontend constants **/

// How long to forcefully delay the rendering of the RedWarn app for - this is utilised to add the "RedWarn" splash screen when loading is fast.
// This serves no useful purpose outside of being aesthetically pleasing. If loading takes longer, lower this time.
export const RW_FRONTEND_SPLASH_DELAY = 1000;

// The wiki we are currently on. DO NOT KEEP! This must be changed later on!
export const RW_FRONTEND_WIKI = "enwiki";

/** Wikipedia-related constants **/

// The cache delay between ORES API checks.
// WARNING: Setting this too high means very slow UI updating and
// maybe even overload the request with too many arguments.
export const RW_RC_DELAY = 1000;

// RC Feed Event Source
export const RW_RC_SOURCE_URL = "https://stream.wikimedia.org/v2/stream/recentchange";

// ORES supported Wikis - Taken from "https://ores.wikimedia.org/v3/scores/"
export const RW_ORES_SUPPORTED_WIKIS = ["arwiki", "bnwiki", "bswiki", "cawiki", "cswiki", "dewiki", "elwiki", "enwiki", "enwiktionary", "eswiki", "eswikibooks", "eswikiquote", "etwiki", "euwiki", "fakewiki", "fawiki", "fiwiki", "frwiki", "frwikisource", "glwiki", "hewiki", "hrwiki", "huwiki", "idwiki", "iswiki", "itwiki", "jawiki", "kowiki", "lvwiki", "nlwiki", "nowiki", "plwiki", "ptwiki", "rowiki", "ruwiki", "simplewiki", "sqwiki", "srwiki", "svwiki", "tawiki", "testwiki", "trwiki", "ukwiki", "viwiki", "wikidatawiki", "zhwiki"];