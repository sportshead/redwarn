import MWRecentChange from "../objects/MWRecentChange";
import {ClientToServerMessage, ServerToClientMessage} from "./WebSocketMessage";

export type RCWebSocketMessages =
    /* Server to Client messages */
    ServerRecentChange

    /* Client to Server messages */
    | ClientSetRCFilters;



export interface ClientSetRCFilters extends ClientToServerMessage {

    type: "setRCFilters";
    wiki: string;

}

export interface ServerRecentChange extends ServerToClientMessage {

    type: "recentChange";
    change: MWRecentChange;

}