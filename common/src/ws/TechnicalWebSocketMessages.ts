import {ClientToServerMessage, ServerToClientMessage} from "./WebSocketMessage";

export type TechnicalWebSocketMessages =
    /* Server to Client messages */
    ServerHandshake
    | ServerError
    | ServerHandshakeDone

    /* Client to Server messages */
    | ClientHandshake;

export interface ServerHandshake extends ServerToClientMessage {

    type: "handshake";
    serverVersion: string;
    serverIdentifier: string;
    serverCommitHash: string;

}

export interface ClientHandshake extends ClientToServerMessage {

    type: "handshake";
    clientVersion: string;
    clientIdentifier: string;
    clientCommitHash: string;

}

export interface ServerError extends ServerToClientMessage {

    type: "error";
    fatal: boolean;
    reason: string;
    extra?: any;

}

export interface ServerHandshakeDone extends ServerToClientMessage {

    type: "handshakedone";
    warnings: string[];

}