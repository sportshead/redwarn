import WebSocket, {ServerOptions} from "ws";
import WSMS from "../../../common/src/ws/WebSocketMessageSerializer";
import {RW_BACKEND_IDENTIFIER, RW_BACKEND_VERSION, RW_FRONTEND_VERSION} from "../../../common/src/Constants";
import {RW_GIT_HASH} from "../../../common/src/DynamicConstants";
import Logger from "bunyan";

import {
    messageMatcherClientToServer,
    Direction
} from "../../../common/src/ws/WebSocketMessage";
import {WebSocketInfo} from "./WebSocketInfo";

export default class WebSocketServer {

    websockets : WebSocketInfo[] = [];

    private _activeWikis : string[] = [];

    get activeWikis() : string[] {
        return this._activeWikis;
    }

    private log : Logger;
    private wss : WebSocket.Server;

    constructor(log : Logger, options : ServerOptions) {
        this.log = log;
        this.wss = new WebSocket.Server(options);

        this.wss.on("connection", (ws) => {
            this.wsConnectionRegister(ws);
        });
        this.wss.on("close", () => {
            this.log.error("RC WebSocket Server closed.");
        });
    }

    /**
     * Asynchronously broadcasts a message to all shards.
     * @param message The message to send.
     * @param filter A filter to test against the websocket before sending.
     */
    async wsBroadcast(message : string, filter? : (wsi : WebSocketInfo) => boolean) : Promise<void> {
        // this.log.trace("Broadcasting message...", {
        //    message: message,
        //    targets: this.websockets.filter(filter).length
        // });
        for (const socketInfo of this.websockets) {
            // noinspection ES6MissingAwait
            (async () => {
                if (socketInfo.authenticated && (filter == null || filter(socketInfo))) {
                    socketInfo.socket.send(message);
                }
            })();
        }
    }

    wsConnectionRegister(ws : WebSocket) : void {
        this.log.info(`Connection established with ${ws.url}`);

        // Await return handshake before acknowledging other messages. Like a start codon (biology).
        // After acknowledging the connection:
        const socketInfo : WebSocketInfo = {
            authenticated: false,
            "socket": ws
        };
        this.websockets.push(socketInfo);

        // Lots of other things on `ws.on`. Just add them in when you need them,
        // and follow the same format.
        ws.on("message", (message: string) => {
            this.wsConnectionMessage(ws, message, socketInfo);
        });

        ws.on("close", () => {
            this.wsConnectionClose(ws);
        });

        ws.send(WSMS.serializeServer({
            direction: Direction.ServerToClient,
            type: "handshake",
            serverVersion: RW_BACKEND_VERSION,
            serverIdentifier: RW_BACKEND_IDENTIFIER,
            serverCommitHash: RW_GIT_HASH,
        }));
    }

    wsConnectionClose(ws : WebSocket) : void {
        this.log.info(`Connection closed with "${ws.url}".`);
        this.websockets = this.websockets.filter(s => s.socket !== ws); // remove our closed socket from the list
    }

    wsConnectionMessage(ws : WebSocket, rawMessage : string, socketInfo : WebSocketInfo) : void {
        // When a connection is made, the socket needs to select a Wiki for filtering,
        // this is done by sending it, if none is defined then traffic from all Wikis will be sent

        // Below matches our socket in the array and swaps its wiki
        const message = WSMS.deserializeServer(rawMessage);

        if (!socketInfo.authenticated && message.type !== "handshake") {
            ws.send(WSMS.serializeServer({
                direction: Direction.ServerToClient,
                type: "error",
                fatal: true,
                reason: "Non-handshake message received before authentication."
            }));
            ws.terminate();
            return;
        }

        messageMatcherClientToServer<void>({
            handshake: (message) => {
                if (message.clientVersion !== RW_FRONTEND_VERSION) {
                    ws.send(WSMS.serializeServer({
                        direction: Direction.ServerToClient,
                        type: "error",
                        fatal: true,
                        reason: "Version mismatch",
                        extra: {
                            clientVersion: message.clientVersion,
                            serverVersion: RW_BACKEND_VERSION,
                            serverIdentifier: RW_BACKEND_IDENTIFIER,
                            serverCommitHash: RW_GIT_HASH
                        }
                    }));
                    ws.close();
                    return;
                }
                socketInfo.authenticated = true;
                if (message.clientCommitHash !== RW_GIT_HASH) {
                    this.log.debug(`Git commit hash mismatch for "${ws.url}". Connection possibly unstable.`);
                }
                ws.send(WSMS.serializeServer({
                    direction: Direction.ServerToClient,
                    type: "handshakedone",
                    warnings: (message.clientCommitHash !== RW_GIT_HASH ? [
                        "Git hash mismatch. Connection may be unstable."
                    ] : [])
                }));
            },
            setRCFilters: (message) => {
                socketInfo.wiki = message.wiki;
                this.log.info(`Websocket "${ws}" changed wiki to: ${socketInfo.wiki}`);

                // Update active wikis
                this._activeWikis = [...new Set(this.websockets.map(ws => ws.wiki))];
            }
        })(message);
    }

}