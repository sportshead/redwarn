import WebSocket from "ws";
import MWRecentChange from "../../../common/src/objects/MWRecentChange";

interface TypedRCFilter { type : string }

interface AlwaysRCFilter extends TypedRCFilter { type: "always" }
interface NeverRCFilter extends TypedRCFilter { type: "never" }
interface AndRCFilter extends TypedRCFilter { type: "and", subfilters: string[] }
interface OrRCFilter extends TypedRCFilter { type: "or", subfilters: string[] }
interface XorRCFilter extends TypedRCFilter { type: "xor", subfilters: string[] }

type RCFilters =
    AlwaysRCFilter
    | NeverRCFilter
    | AndRCFilter
    | OrRCFilter
    | XorRCFilter;

type RCFilterTypes = RCFilters["type"];
type RCFilterResolvers = {
    [key in RCFilterTypes]: (...args: any[]) => boolean;
};

export interface WebSocketInfo {

    // Might add token info, etc. related to socket here
    authenticated: boolean;
    socket: WebSocket;

    rcFilters?: RCFilter[];

}