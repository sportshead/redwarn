import ResolvingRecentChangesDecorator, {RecentChangePromiseSet} from "./ResolvingRecentChangesDecorator";
import MWRecentChange from "../../../../common/src/objects/MWRecentChange";
import Axios from "axios";
import UserData from "../../../../common/src/objects/UserData";
import {UserExpansion} from "../../../../common/src/objects/ExpandedMWRecentChange";

export default class UserDecorator extends ResolvingRecentChangesDecorator<UserExpansion> {

    public static decoratorName = "dec_user";

    // { wiki: { revid: promise of expanded data } }
    protected resolutionQueue: Record<string, Record<string, RecentChangePromiseSet<UserExpansion>>> = {};

    async decorateChange(rawChange: MWRecentChange): Promise<UserExpansion> {
        return this.addChangeToResolutionQueue(rawChange);
    }

    protected async addChangeToResolutionQueue(rawChange: MWRecentChange): Promise<UserExpansion> {
        if (this.resolutionQueue[rawChange.wiki] === undefined)
            this.resolutionQueue[rawChange.wiki] = {};

        this.resolutionQueue[rawChange.wiki][rawChange.id] = {
            change: rawChange,
            ...this.createDecoratorPromise({
                isDecoratorError: true,
                decorator: UserDecorator.decoratorName,
                wiki: rawChange.wiki,
                revid: rawChange.revision.new,
                title: rawChange.title
            })
        };
        return this.resolutionQueue[rawChange.wiki][rawChange.id].promise;
    }

    protected async resolveQueue(): Promise<void> {
        this.log.trace("Resolving...", { decorator: UserDecorator.decoratorName });
        for (const wiki of Object.keys(this.resolutionQueue)) {
            const revisions = this.resolutionQueue[wiki];
            if (Object.keys(revisions).length === 0) continue;
            this.resolutionQueue[wiki] = {};
            // `Set` will remove all duplicates from the array.
            const userNames = [...new Set(
                Object.values(revisions)
                    .map((v : { change: MWRecentChange }) => v.change.user)
            )];
            try {
                // We can assume that all of the server URLs are the same since they
                // all use the same WMF database name.
                const wikiAPIURL =
                    `${
                        Object.values(revisions)[0].change.server_url
                    }${
                        Object.values(revisions)[0].change.server_script_path
                    }/api.php`;

                // Request user info from MediaWiki API of that wiki.
                const userData : UserData[] = (await Axios.get(wikiAPIURL, {
                    params: {
                        "format" : "json",
                        "action" : "query",
                        "list": "users",

                        "ususers": userNames.join("|"), // join together with pipe for multiparam
                        "usprop": [
                            "groups",
                            "editcount"
                        ].join("|") // get these properties
                    },
                    responseType: "json"
                })).data.query.users;

                for (const promiseSet of Object.values(revisions)) {
                    promiseSet.resolver({
                        userInfo: userData.find(u => u.name === promiseSet.change.user)
                    });
                }
            } catch (e) {
                this.log.error(e, "An error occurred in attempting to get user information for revisions.");
                for (const change of Object.values(revisions)) {
                    change.rejector({
                        isDecoratorError: true,
                        decorator: UserDecorator.decoratorName,
                        requested: userNames,
                        ...e
                    });
                }
            }
        }
    }

}