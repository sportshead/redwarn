import MWRecentChange from "../../../../common/src/objects/MWRecentChange";
import RecentChangesDecorator from "./RecentChangesDecorator";
import {ByteDiffExpansion} from "../../../../common/src/objects/ExpandedMWRecentChange";

export default class ByteDiffDecorator extends RecentChangesDecorator<ByteDiffExpansion> {

    public static decoratorName = "dec_bytes";

    async decorateChange(rawChange: MWRecentChange): Promise<ByteDiffExpansion> {
        return {
            ...rawChange,
            bytes: rawChange.length.new - rawChange.length.old
        };
    }

}