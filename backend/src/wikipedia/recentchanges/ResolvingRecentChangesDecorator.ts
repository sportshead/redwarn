import MWRecentChange from "../../../../common/src/objects/MWRecentChange";
import Logger from "bunyan";
import Timeout = NodeJS.Timeout;
import {RW_RC_DELAY} from "../../../../common/src/Constants";
import RecentChangesDecorator from "./RecentChangesDecorator";

export interface RecentChangePromiseSet<T> {

    change: MWRecentChange;
    promise: Promise<T>;
    resolver: (resolvedChange: T) => void;
    rejector: (error: Error) => void;

}

export default abstract class ResolvingRecentChangesDecorator<T> extends RecentChangesDecorator<T> {

    protected abstract resolutionQueue : any;

    private resolutionLoop : Timeout;

    constructor(log: Logger) {
        super(log);

        this.startResolutionLoop();
    }

    stopResolutionLoop() : void {
        clearInterval(this.resolutionLoop);
    }

    startResolutionLoop() : void {
        this.resolutionLoop = setInterval(() => {
            // Because "this".
            this.resolveQueue();
        }, RW_RC_DELAY);
    }

    /**
     * Adds a Wikimedia recent change to the resolution queue for resolution.
     *
     * @param rawChange A raw Wikimedia change for resolution.
     * @returns A promise of the fully-resolved change.
     **/
    protected abstract async addChangeToResolutionQueue(rawChange: MWRecentChange) : Promise<T>;

    /**
     * Repeatedly called by a timer function - the resolution function is responsible
     * for assigning data into one or multiple recent changes.
     **/
    protected abstract async resolveQueue() : Promise<void>;

    /**
     * Creates a promise that can either be resolved, rejected, or timed out internally.
     * @param jicErrorInfo Just-In-Case Error information. For logging just in case the promise times out.
     */
    createDecoratorPromise(jicErrorInfo? : Record<string, any>) : {
        promise: Promise<T>,
        resolver: (resolvedChange: T) => void,
        rejector: (error: Error) => void
        } {
        let resolver : (resolvedChange: T) => void = null;
        let rejector : (error: Error) => void = null;
        return {
            promise: new Promise<T>(
                (res, rej) => {
                    resolver = res;
                    rejector = rej;

                    // Timeout to avoid forever held-back changes.
                    setTimeout(() => {
                        rej({
                            timeout: true,
                            ...(new Error("Decoration promise took too long.")),
                            ...jicErrorInfo
                        });
                    }, 4000);
                }
            ),
            resolver: resolver,
            rejector: rejector
        };
    }

}