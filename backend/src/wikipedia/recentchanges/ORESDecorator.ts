import ResolvingRecentChangesDecorator, {RecentChangePromiseSet} from "./ResolvingRecentChangesDecorator";
import MWRecentChange from "../../../../common/src/objects/MWRecentChange";
import ORESData from "../../../../common/src/objects/ORESData";
import Axios from "axios";
import {RW_ORES_SUPPORTED_WIKIS} from "../../../../common/src/Constants";
import {ORESExpansion} from "../../../../common/src/objects/ExpandedMWRecentChange";

export default class ORESDecorator extends ResolvingRecentChangesDecorator<ORESExpansion> {

    public static decoratorName = "dec_ores";

    // { wiki: { revid: promise of expanded data } }
    protected resolutionQueue: Record<string, Record<string, RecentChangePromiseSet<ORESExpansion>>> = {};

    async decorateChange(rawChange: MWRecentChange): Promise<ORESExpansion> {
        if (RW_ORES_SUPPORTED_WIKIS.includes(rawChange.wiki))
            return this.addChangeToResolutionQueue(rawChange);
        else {
            return {
                ...rawChange,
                ores: false
            };
        }
    }

    protected async addChangeToResolutionQueue(rawChange: MWRecentChange): Promise<ORESExpansion> {
        if (this.resolutionQueue[rawChange.wiki] === undefined)
            this.resolutionQueue[rawChange.wiki] = {};

        this.resolutionQueue[rawChange.wiki][rawChange.revision.new] = {
            change: rawChange,
            ...this.createDecoratorPromise({
                isDecoratorError: true,
                decorator: ORESDecorator.decoratorName,
                wiki: rawChange.wiki,
                revid: rawChange.revision.new,
                title: rawChange.title
            })
        };
        return this.resolutionQueue[rawChange.wiki][rawChange.revision.new].promise;
    }

    protected async resolveQueue(): Promise<void> {
        this.log.trace("Resolving...", { decorator: ORESDecorator.decoratorName });
        for (const wiki of Object.keys(this.resolutionQueue)) {
            const revisions = this.resolutionQueue[wiki];
            if (Object.keys(revisions).length === 0) continue;
            this.resolutionQueue[wiki] = {};
            const idsForORES = Object.values(revisions).map(rev => rev.change.revision.new);
            try {
                const oresScores : Record<string, ORESData> =
                    (await Axios.get(`https://ores.wikimedia.org/v3/scores/${wiki}`, {
                        params: {
                            "revids" : idsForORES.join("|")
                        },
                        responseType: "json"
                    })).data[wiki].scores;

                for (const [id, oresData] of Object.entries(oresScores)) {
                    const expandedChange : ORESExpansion = {
                        ores: oresData
                    };
                    revisions[id].resolver(expandedChange);
                }
            } catch (e) {
                this.log.error(e, "An error occurred in attempting to get ORES scores for revisions.");
                for (const change of Object.values(revisions)) {
                    change.rejector({
                        isDecoratorError: true,
                        decorator: ORESDecorator.decoratorName,
                        requested: idsForORES,
                        ...e
                    });
                }
            }
        }
    }

}