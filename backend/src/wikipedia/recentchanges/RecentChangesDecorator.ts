import Logger from "bunyan";
import MWRecentChange from "../../../../common/src/objects/MWRecentChange";

export default abstract class RecentChangesDecorator<T> {

    protected log : Logger;

    constructor(log : Logger) {
        this.log = log;
    }

    /**
     * Decorates a raw Wikimedia recent change with other data
     *
     * @param rawChange A raw Wikimedia change for resolution.
     * @returns A promise of the fully-resolved change.
     **/
    abstract async decorateChange(rawChange: MWRecentChange): Promise<T>;

}