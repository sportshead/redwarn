import path from "path";
import express from "express";
import Logger from "bunyan";
import moment from "moment";
import * as http from "http";
import { AddressInfo } from "net";
import {
    RW_BACKEND_IDENTIFIER,
    RW_DEBUG
} from "../../common/src/Constants";
import {RW_ROOT_PATH} from "../../common/src/backend/BackendConstants";
import WebSocketServer from "./websocket/WebSocketServer";
import ORESDecorator from "./wikipedia/recentchanges/ORESDecorator";
import RecentChangesObserver from "./events/RecentChangesObserver";
import UserDecorator from "./wikipedia/recentchanges/UserDecorator";
import ByteDiffDecorator from "./wikipedia/recentchanges/ByteDiffDecorator";

/**
 * The RedWarn Backend server. This is where the magic happens.
 */
export default class RedWarnBackendServer {

    /** The `bunyan` logger. **/
    public log : Logger;
    /** The timestamp when the RWB Server started. **/
    startTime : number;

    /** The Express application **/
    private app : express.Express;

    /** The {@link WebSocketServer} for this RedWarn Backend server. **/
    private recentChangesWebsocketServer : WebSocketServer;

    /**
     *  A collection of observers mapped by name.
     *
     *  An observer is a class which either listens to events or regularly checks an
     *  endpoint, and updates whatever components need updating when the observer
     *  detects a change.
     **/
    private observers: Record<string, any> = {}; // No parent class for observers yet.

    /**
     *  The HTTP listener.
     *
     *  Note to homebrew RedWarn operators: If you want RedWarn to use HTTPS,
     *  you can do that by using {@link https.Server} instead.
     **/
    private httpListener? : http.Server;
    /**
     * The RedWarn routers.
     *
     * Each router is responsible for an action on a certain group of endpoints. For
     * exaple, all request in the `/api` path will be passed to the API router. This
     * way, certain middleware can be isolated to specific parts of the server.
     **/
    private routers : { [key : string] : express.Router } = {};

    /**
     * Creates a new RedWarnBackendServer.
     */
    constructor() {
        // Set the `bunyan` log.
        // Still debating whether a filestream log is required.
        this.log = Logger.createLogger({
            name: "RedWarn Backend",
            src: true,
            streams: [
                {
                    level: RW_DEBUG ? "trace" : "info",
                    stream: process.stdout
                }
            ]
        });
    }

    /**
     * Verify environment variables.
     *
     * @returns `true` on success, `string` with reason on failure.
     */
    verifyEnvironment() : true | string {
        if (RW_ROOT_PATH == null)
            return "RedWarn root path cannot be found. Is RedWarn in the right directory, or is the anchor file present?";
        return true;
    }

    /**
     * Start up the Express routers to be used.
     */
    async initializeExpressRouters() : Promise<express.Router[]> {
        this.routers["socket_fallback"] = express.Router();

        this.routers["socket_fallback"]
            .get("/ws", (req, res) => {
                res.status(503);
                res.send();
            });

        return Object.values(this.routers);
    }

    /**
     * Creates an Express app and assigns the proper middleware.
     */
    async initializeExpressServer() : Promise<void> {
        this.app = express();

        //this.app.use(expressUserAgent.express());
        //this.app.use(cookieParser());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));

        for (const router of (await this.initializeExpressRouters()))
            this.app.use(router);

        this.app.use("/", express.static(path.resolve(RW_ROOT_PATH, "static")));
    }

    async initializeWebsocketServer() : Promise<void> {

        this.recentChangesWebsocketServer = new WebSocketServer(this.log, {
            server: this.httpListener,
            path: "/ws/recent_changes"
        });

    }

    async initializeWikipediaObservers() : Promise<void> {
        this.observers["recent_changes"] =
            new RecentChangesObserver(
                this.log,
                this.recentChangesWebsocketServer,
                [
                    new ByteDiffDecorator(this.log),
                    new ORESDecorator(this.log),
                    new UserDecorator(this.log)
                ]
            );

        this.observers["recent_changes"].beginListening();
    }

    /**
     * Starts the RedWarn Backend Server.
     */
    async startup() : Promise<void> {
        this.startTime = Date.now();
        this.log.info(`[!] RedWarn Backend Server started on ${moment().format()}.`);
        this.log.info(`RedWarn Backend v${RW_BACKEND_IDENTIFIER}`);

        let environmentOK;
        if ((environmentOK = this.verifyEnvironment()) !== true) {
            this.log.fatal(`Cannot continue startup. Reason: ${environmentOK}`);
        }

        // Create the Express app and respective routers.
        this.log.info("Creating the Express server...");
        await this.initializeExpressServer();

        // Wikimedia Cloud will automatically encrypt internet requests, so we
        // don't need to worry about running HTTPS. This is a HTTP server that
        // only runs locally and is normally firewalled anyway
        this.log.info(`Running HTTP listener on port ${process.env.PORT ?? 45990}...`);
        this.httpListener = http.createServer(this.app);
        this.httpListener.listen(process.env.PORT ?? 45990, async () => {
            this.log.info(`HTTP is listening on port ${
                (this.httpListener.address() as AddressInfo).port
            }.`);
        });

        // Create the WSS after the thing initializes.
        await this.initializeWebsocketServer();

        // Create all Wikipedia-related listeners and the like.
        await this.initializeWikipediaObservers();
    }

    /**
     * Stops the RedWarn Backend Server.
     */
    async shutdown() : Promise<void> {
        if (this.httpListener != null && this.httpListener.listening) {
            this.log.info("Shutting down web server...");

            if (this.httpListener != null)
                this.httpListener.close();

            this.log.info("Web server closed.");
        }

        this.log.info();
        this.log.info(`Total Execution Time: ${moment(Date.now() - this.startTime).format()}`);
        this.log.info();
        this.log.info(`Shutdown complete (${moment().format()})`);
    }

}

// main code
const rwBackendServer = new RedWarnBackendServer();
rwBackendServer.startup();