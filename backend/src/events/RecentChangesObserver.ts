import WebSocketServer, {WebSocketInfo} from "../websocket/WebSocketServer";
import MWRecentChange from "../../../common/src/objects/MWRecentChange";
import WSMS from "../../../common/src/ws/WebSocketMessageSerializer";
import {Direction} from "../../../common/src/ws/WebSocketMessage";
import Logger from "bunyan";
import {RW_RC_SOURCE_URL} from "../../../common/src/Constants";
import EventSource from "eventsource";
import RecentChangesDecorator from "../wikipedia/recentchanges/RecentChangesDecorator";

// This is in sync with standard EventSource ready states.
enum RecentChangesObserverStatus {
    Pending = -1,
    Connecting,
    Open,
    Closed
}

export default class RecentChangesObserver {

    eventSource : EventSource;

    private log : Logger;
    private callbacks : ((change : MWRecentChange) => void)[] = [];
    private readonly decorators : RecentChangesDecorator<any>[] = [];

    private wss : WebSocketServer;

    get status() : RecentChangesObserverStatus {
        return this.eventSource == null ? -1 : this.eventSource.readyState;
    }

    constructor(
        log: Logger,
        wss: WebSocketServer,
        decorators: RecentChangesDecorator<any>[],
        autoListen = false
    ) {
        this.log = log;
        this.wss = wss;
        this.decorators = decorators;

        if (autoListen) {
            this.beginListening();
        }
    }

    public addObserverCallback(callback : (change : MWRecentChange) => void) : number {
        return this.callbacks.push(callback);
    }

    public removeObserverCallback(callback : (change : MWRecentChange) => void) : void {
        this.callbacks = this.callbacks.filter(c => c !== callback);
    }

    beginListening() : void {
        this.eventSource = new EventSource(RW_RC_SOURCE_URL);

        this.eventSource.addEventListener("open", () => {
            this.log.info("Wikimedia Recent Changes feed opened.");
        });
        this.eventSource.addEventListener("error", (e : ErrorEvent) => {
            this.log.error(e, "An issue occurred while opening the WM Recent Changes feed.");
        });

        // Since each change is handled asynchronously from each other, this function will
        // always wait until it has been fully decorated, and decorations will come from
        // the resolutions inside of Decorator classes.
        //
        // In summary: We wait until we have all extra data injected into the change, and
        // then we push it to the user.
        this.eventSource.addEventListener("message", async (event : MessageEvent) => {
            let editInfo : MWRecentChange;
            try {
                editInfo = JSON.parse(event.data);
            } catch (e) {
                this.log.error({
                    data: event.data
                }, "Unexpected data from WMRC. Data will be disregarded.");
            }

            // Skip wikis without listeners.
            if (!this.wss.activeWikis.includes(editInfo.wiki))
                return;

            // if not edit type, skip. Other type intergration TODO.
            if (editInfo.type !== "edit")
                return;

            this.log.trace("RC Hit!", {
                wiki: editInfo.wiki,
                page: editInfo.title,
                user: editInfo.user
            });

            /*
             * If you want to insert data into the recent change, do it as a Decorator.
             * Otherwise, we'll just have a large pool of `any`, and TypeScript would have
             * been useless.
             */

            const decorationPromises = [];
            let decoratedInfo = editInfo;

            // Execute all decoration promises asynchronously.
            for (const decorator of this.decorators) {
                decorationPromises.push(decorator.decorateChange(decoratedInfo)
                    .catch((error) => {
                        this.log.debug(error, "Promise rejected with an error. Change will be untouched.");
                        return decoratedInfo;
                    }));
            }

            decoratedInfo = Object.assign((await Promise.all(
                decorationPromises
            )).reduce((prev, next) => { return {...prev, ...next}; }, {}));

            await this.wss.wsBroadcast(
                WSMS.serializeServer({
                    direction: Direction.ServerToClient,
                    type: "recentchange",
                    change: decoratedInfo
                }),
                (wsi : WebSocketInfo) => wsi.wiki === decoratedInfo.wiki
            );
        });
    }

    stopListening() : void {
        this.eventSource.close();
        this.log.info("WM Recent Changes feed observer closed.");
    }

}