"use strict";

/* eslint-disable @typescript-eslint/no-var-requires */
const { exec } = require("child_process");
const path = require("path");
const fs = require("fs");

(async () => {
    const begin = Date.now();

    const srcDir = (() => {
        const root = path.resolve("/");
        let src = path.resolve(__dirname);

        let ok = false;
        do {
            if (
                fs.existsSync(path.join(src, "common"))
                && fs.existsSync(path.join(src, "static"))
                && fs.existsSync(path.join(src, "backend"))
                && fs.existsSync(path.join(src, "frontend"))
            ) {
                ok = true;
                break;
            } else {
                src = path.resolve(src, "..");
            }
        } while (src !== root);

        if (!ok) {
            console.error("Looks like we couldn't find the root folder. Is the script in the right place?");
            process.exit(124);
        }

        return src;
    })();
    const commonsDir = path.join(srcDir, "common");

    const runProcess = async (command, options, runOptions = {
        silent: false
    }) => {
        if (!runOptions.silent) console.log("-".repeat(60));
        const proc = exec(command, {
            cwd: srcDir,
            windowsHide: true,
            ...options
        });

        let stdout = Buffer.alloc(0);
        let stderr = Buffer.alloc(0);

        // noinspection JSUnresolvedFunction
        proc.stdout.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stdout.write(d);

            stdout = Buffer.concat([stdout, data]);
        });

        // noinspection JSUnresolvedFunction
        proc.stderr.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stderr.write(d);

            stderr = Buffer.concat([stderr, data]);
        });

        const code = await new Promise((res, rej) => {
            proc.on("exit", function (code) {
                if (code === 0) res(code);
                else rej(code);
            });
        }).catch(r => r);

        if (!runOptions.silent) console.log("-".repeat(60));
        return {
            code: code,
            stdout: stdout,
            stderr: stderr
        };
    };

    // Introduction
    console.log();
    console.log(" ---------------------------- RedWarn Setup Script  ---------------------------- ");
    console.log();
    console.log("(c) 2020 The RedWarn contributors.");
    console.log("Licensed under the Apache License 2.0 - read more at https://gitlab.com/redwarn");
    console.log();

    // Determine git commit
    const gitHash = (await runProcess("git rev-parse HEAD", {}, {
        silent: true
    })).stdout.toString("utf-8").trim();

    console.log("Setting constants...");
    console.log();

    fs.writeFileSync(
        path.resolve(commonsDir, "src", "DynamicConstants.ts"),
        `export const RW_GIT_HASH = "${gitHash}";`
    );

    console.log("Downloading common dependencies...");
    await runProcess("npm i --no-progress", { cwd: path.resolve(srcDir, "common") });

    console.log("Downloading frontend dependencies...");
    await runProcess("npm i --no-progress", { cwd: path.resolve(srcDir, "frontend") });

    console.log("Downloading backend dependencies...");
    await runProcess("npm i --no-progress", { cwd: path.resolve(srcDir, "backend") });

    for (let i = 0; i < 10; i++) console.log();

    console.log(`Build successfully completed after ${(Date.now() - begin) / 1000}s.`);
    console.log("Happy patrolling! ~~~~");
})();