"use strict";

/* eslint-disable @typescript-eslint/no-var-requires */
const { exec } = require("child_process");
const path = require("path");
const fs = require("fs");

(async () => {
    const begin = Date.now();

    const rootDir = (() => {
        const root = path.resolve("/");
        let currentPath = path.resolve(__dirname);

        while (!fs.existsSync(path.join(currentPath, ".redwarn-anchor"))) {
            if (currentPath === root)
                return null;
            else
                currentPath = path.resolve(currentPath, "..");
        }

        return currentPath;
    })();
    const commonsDir = path.join(rootDir, "common");

    const deleteFolderRecursive = function(dir) {
        if (fs.existsSync(dir)) {
            fs.readdirSync(dir).forEach((file) => {
                const curPath = path.join(dir, file);
                if (fs.lstatSync(curPath).isDirectory()) {
                    deleteFolderRecursive(curPath);
                } else {
                    console.log(" ".repeat(8) + curPath);
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(dir);
        }
    };
    const runProcess = async (command, options, runOptions = {
        silent: false
    }) => {
        if (!runOptions.silent) console.log("-".repeat(60));
        const proc = exec(command, {
            cwd: rootDir,
            windowsHide: true,
            ...options
        });

        let stdout = Buffer.alloc(0);
        let stderr = Buffer.alloc(0);

        // noinspection JSUnresolvedFunction
        proc.stdout.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stdout.write(d);

            stdout = Buffer.concat([stdout, data]);
        });

        // noinspection JSUnresolvedFunction
        proc.stderr.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stderr.write(d);

            stderr = Buffer.concat([stderr, data]);
        });

        const code = await new Promise((res, rej) => {
            proc.on("exit", function (code) {
                if (code === 0) res(code);
                else rej(code);
            });
        }).catch(r => r);

        if (!runOptions.silent) console.log("-".repeat(60));
        return {
            code: code,
            stdout: stdout,
            stderr: stderr
        };
    };

    // Introduction
    console.log();
    console.log(" ---------------------------- RedWarn Clean Script  ---------------------------- ");
    console.log();
    console.log("(c) 2020 The RedWarn contributors.");
    console.log("Licensed under the Apache License 2.0 - read more at https://gitlab.com/redwarn");
    console.log();
    console.log("This will clean up all build files and articles. If you want a full reset,");
    console.log("which includes wiping all downloaded packages, you should just re-clone the repo.");
    console.log();

    // Determine git commit
    const gitHash = (await runProcess("git rev-parse HEAD", {}, {
        silent: true
    })).stdout.toString("utf-8").trim();

    console.log("Updating constants...")
    console.log();

    fs.writeFileSync(
        path.resolve(commonsDir, "src", "DynamicConstants.ts"),
        `export const RW_GIT_HASH = "${gitHash}";`
    );

    console.log("Deleting build artifacts...");
    console.log("   - Backend artifacts...");
    deleteFolderRecursive(path.resolve(rootDir, "backend", "build"));
    console.log("   - Frontend artifacts...");
    deleteFolderRecursive(path.resolve(rootDir, "frontend", "build"));
    const rwStaticScripts = path.resolve(rootDir, "static", "scripts");
    fs.readdirSync(rwStaticScripts).forEach(v => {
        if (/redwarn-v[0-9.dp]+?-[a-f0-9]+?.js/.test(v))
            fs.unlinkSync(path.resolve(rwStaticScripts, v));
    });
    fs.unlinkSync(path.resolve(rootDir, "static", "scripts", "redwarn.js"));

    console.log();
    console.log();

    console.log(`Cleaned successfully completed after ${(Date.now() - begin) / 1000}s.`);
})();