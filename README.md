# RedWarn
RedWarn's core app - made with Typescript and React.

The [**R**ecent **Ed**its Patrol and **Warn**ing Tool](https://en.wikipedia.org/wiki/WP:RedWarn) (abbreviated as RedWarn) is a [Wikipedia](https://en.wikipedia.org/wiki/) [patrol](https://en.wikipedia.org/wiki/WP:RCP) and [counter-vandalism](https://en.wikipedia.org/wiki/WP:VD) tool, designed to be a user-friendly way to perform common moderation tasks.

This repository contains both frontend and backend. You can find them in their respective folders (`frontend`, and `backend`). Both ends rely on the `common` folder for common definitions.

## Contributors
RedWarn is primarily maintained and developed by the RedWarn contributors.

* **[@ed_E](https://gitlab.com/ed_e)** ([\[\[User:Ed6767\]\]](https://en.wikipedia.org/wiki/User:Ed6767)) - lead developer, designer, and chief maintainer
* **[@pr0mpted](https://gitlab.com/pr0mpted)** - ([\[\[User:Prompt0259\]\]](https://en.wikipedia.org/wiki/User:Prompt0259)) - additional development and design
* **[@ChlodAlejandro](https://gitlab.com/ChlodAlejandro)** - ([\[\[User:Chlod\]\]](https://en.wikipedia.org/wiki/User:Chlod)) - additional development and design
* **[and everyone else on the RedWarn team.](https://gitlab.com/groups/redwarn/-/group_members)**

## Development
If you wish to contribute in the development of RedWarn, follow the instructions on the [CONTRIBUTING.md](https://gitlab.com/redwarn/redwarn/-/blob/master/CONTRIBUTING.md) file.