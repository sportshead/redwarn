FROM node:12
# Please inform us if there's any issues with this Dockerfile, either in
# the RedWarn Discord server or on the RedWarn talk page on Wikipedia.

# Resolve dependencies
RUN npm i -g typescript
RUN mkdir /tmp/frontend && mkdir /tmp/backend

## Deps (Frontend)
COPY frontend/package*.json /app/frontend/
RUN cd /app/frontend && npm i

## Deps (Backend)
COPY backend/package*.json /app/backend/
RUN cd /app/backend && npm i

# Setup project files
COPY . /app
RUN cd /app && node scripts/build.js

# Expose RedWarn port
ENV PORT 45990
EXPOSE 45990

# Start command
WORKDIR /app/backend

ENV TZ utc
CMD [ "npm", "start" ]