"use strict";

/* eslint-disable @typescript-eslint/no-var-requires */
const { exec } = require("child_process");
const path = require("path");
const fs = require("fs");

module.exports = (async () => {
    const begin = Date.now();

    const rootDir = (() => {
        const root = path.resolve("/");
        let currentPath = path.resolve(__dirname);

        while (!fs.existsSync(path.join(currentPath, ".redwarn-anchor"))) {
            if (currentPath === root)
                return null;
            else
                currentPath = path.resolve(currentPath, "..");
        }

        return currentPath;
    })();
    const srcDir = path.resolve(rootDir, "frontend");
    const commonsDir = path.resolve(rootDir, "common");


    const runProcess = async (command, options, runOptions = {
        silent: false
    }) => {
        if (!runOptions.silent) console.log("-".repeat(60));
        const proc = exec(command, {
            cwd: srcDir,
            windowsHide: true,
            ...options
        });

        let stdout = Buffer.alloc(0);
        let stderr = Buffer.alloc(0);

        // noinspection JSUnresolvedFunction
        proc.stdout.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stdout.write(d);

            stdout = Buffer.concat([stdout, data]);
        });

        // noinspection JSUnresolvedFunction
        proc.stderr.on("data", function (d) {
            const data = typeof d === "string" ? Buffer.from(d) : d;

            if (!runOptions.silent)
                // noinspection JSUnresolvedFunction
                process.stderr.write(d);

            stderr = Buffer.concat([stderr, data]);
        });

        const code = await new Promise((res, rej) => {
            proc.on("exit", function (code) {
                if (code === 0) res(code);
                else rej(code);
            });
        }).catch(r => r);

        if (!runOptions.silent) console.log("-".repeat(60));
        return {
            code: code,
            stdout: stdout,
            stderr: stderr
        };
    };

    // Introduction
    if (require.main === module) {
        console.log();
        console.log(" ---- RedWarn Build Script ---- ");
        console.log("         (for frontend)         ");
        console.log(" ------------------------------ ");
        console.log();
    }

    // Determine RedWarn version
    const rwVersion = (/RW_FRONTEND_VERSION\s?=\s?"(.*)";/g
        .exec(fs.readFileSync(path.resolve(commonsDir, "src", "Constants.ts")).toString("utf-8")))[1];

    // Determine git commit
    const gitHash = (await runProcess("git rev-parse HEAD", {}, {
        silent: true
    })).stdout.toString("utf-8").trim();

    const rwIdentifier = `${rwVersion}${
        process.env.NODE_ENV === "production" ? "p" : "d"
    }-${gitHash.substr(0, 6)}`;

    console.log(`Building for commit: ${gitHash.trim()}...`);
    console.log(`RedWarn (frontend) v${rwIdentifier}`);
    console.log();

    if (require.main === module) {
        console.log("Updating constants...");
        console.log();

        fs.writeFileSync(
            path.resolve(commonsDir, "src", "DynamicConstants.ts"),
            `export const RW_GIT_HASH = "${gitHash}";`
        );
    }

    // Change package.json version
    const packageJsonPath = path.resolve(srcDir, "package.json");
    fs.writeFileSync(packageJsonPath,
        fs.readFileSync(packageJsonPath)
            .toString("utf-8")
            .replace(/("version":\s?").+?(")/, `$1${rwVersion}$2`)
    );

    // npm dependency check
    console.log("Checking dependencies...");

    const npmDepCheck = (await runProcess("npm i --no-progress")).code;

    if (npmDepCheck !== 0) {
        console.error(`npm finished with code: \`${npmDepCheck}\`.`);
        console.error("Something bad must have happened. Please fix the issue before rebuilding.");

        process.exit();
    } else {
        console.log("Dependencies all OK.");
    }

    // Webpack
    console.log("(Web)packing things up...");

    const wpPackCode = (await runProcess("npm run webpack")).code;

    if (wpPackCode !== 0) {
        console.error(`Webpack finished with code: \`${wpPackCode}\`.`);
        console.error("Something bad must have happened. Please fix the issue before rebuilding.");

        process.exit();
    } else {
        console.log("Webpack finished successfully.");
    }

    console.log();
    console.log("Artifacts created. Swapping out old RedWarn on static...");
    console.log();

    // Create new RedWarn file.
    const rwJsHeader = Buffer.from(
        [
            "/**",
            " * ",
            " * RedWarn - the modern counter-vandalism tool for Wikipedians.",
            " * ",
            ` * rw-version = ${rwIdentifier}`,
            " * ",
            " * (c) 2020 The RedWarn contributors.",
            " * Licensed under the Apache License 2.0 - read more at https://gitlab.com/redwarn",
            " * ",
            "**/",
            ""
        ].join("\n")
    );
    const rwStaticScripts = path.resolve(rootDir, "static", "scripts");
    if (!fs.existsSync(rwStaticScripts))
        fs.mkdirSync(rwStaticScripts);

    const rwStaticJsPath = path.resolve(rwStaticScripts, `redwarn-v${rwIdentifier}.js`);
    fs.writeFileSync(rwStaticJsPath,
        Buffer.concat([rwJsHeader, fs.readFileSync(
            path.resolve(srcDir, "build", "redwarn.js")
        )]),
        { encoding: "utf-8" }
    );

    // Swap out RedWarn file in static.
    const rwScriptLoader = path.resolve(rwStaticScripts, "redwarn.js");
    fs.writeFileSync(rwScriptLoader,
        [
            "/**",
            " *",
            " * RedWarn - the modern counter-vandalism tool for Wikipedians.",
            " *",
            " * Loading Script. This script will automatically load the latest RedWarn version.",
            " *",
            " * (c) 2020 The RedWarn contributors.",
            " * Licensed under the Apache License 2.0 - read more at https://gitlab.com/redwarn",
            " *",
            " **/",
            "",
            "(() => {",
            "    const rw_script = document.getElementById(\"redwarn-script\");",
            "",
            "    rw_script.id = undefined;",
            "    rw_script.parentElement.removeChild(rw_script);",
            "",
            "    const redwarn = document.createElement(\"script\");",
            `    redwarn.src = "scripts/redwarn-v${rwIdentifier}.js";`,
            `    redwarn.setAttribute("data-rw-commit", "${gitHash}");`,
            "    document.getElementsByTagName(\"head\")[0].appendChild(redwarn);",
            "})();"
        ].join("\n")
    );

    // Delete other RedWarn JS files.
    //
    // This is performed after creating the file so if anyone reloads so we can
    // avoid the infinitesimal chance that someone would load RedWarn while the
    // old RedWarn had just been deleted.
    fs.readdirSync(rwStaticScripts).forEach(v => {
        if (v !== `redwarn-v${rwIdentifier}.js`
            && /redwarn-v[0-9.dp]+?-[a-f0-9]+?.js/.test(v))
            fs.unlinkSync(path.resolve(rwStaticScripts, v));
    });

    if (require.main === module) {
        for (let i = 0; i < 10; i++) console.log();

        console.log(`Build successfully completed after ${(Date.now() - begin) / 1000}s.`);
        console.log("Happy patrolling! ~~~~");
    }
})();