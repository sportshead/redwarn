export interface RedWarnSettings {

    activeWikis: string;

}

export const RW_DEFAULT_SETTINGS : RedWarnSettings = {

    activeWikis: "enwiki"

};

export default class SettingsManager {

    public static settings : RedWarnSettings;

    static async loadSettings() : Promise<void> {
        // Load settings from WP or the database.

        // For now, we'll use the defaults.
        SettingsManager.settings = RW_DEFAULT_SETTINGS;
    }

}