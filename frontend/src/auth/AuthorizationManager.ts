export default class AuthorizationManager {

    // Once we're done with OAuth, modify this class.
    //
    // I'm also gonna have to ask the other developers to work on this, as I obviously have no access
    // to the OAuth keys for RedWarn.
    //
    // Also, don't forget the do's and don'ts of saving OAuth keys.
    // - CA

    /**
     * Checks if the current user is authorized and has an available access key in storage.
     *
     * @returns `true` if the current user is authorized, `false` if not.
     **/
    public isAuthorized() : boolean {
        return true; // Testing value.
    }

}