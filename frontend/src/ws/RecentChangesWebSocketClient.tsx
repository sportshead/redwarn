import WSMS from "../../../common/src/ws/WebSocketMessageSerializer";
import {Direction, messageMatcherServerToClient} from "../../../common/src/ws/WebSocketMessage";
import {RW_FRONTEND_IDENTIFIER, RW_FRONTEND_VERSION} from "../../../common/src/Constants";
import {RW_GIT_HASH} from "../../../common/src/DynamicConstants";
import SettingsManager from "../settings/SettingsManager";
import RecentChangesPatrolManager from "../patrol/RecentChangesPatrolManager";
import RedWarnUI from "../ui/RedWarnUI";

/**
 * This class handles all WebSocket communication with the RedWarn Backend (RWB).
 *
 * All of the messages serialized and deserialized here are defined the in RW
 * Common files. This makes standardization between client and server easy.
 *
 * This should be loaded in as soon as the page is loaded, since there are
 * controls that can adjust what or how much data is transferred.
 */
export default class RecentChangesWebSocketClient {

    /** The websocket connection with RWB. **/
    ws : WebSocket;
    /** The RecentChangesPatrolManger handling the recent changes. **/
    patrolManager : RecentChangesPatrolManager;

    /**
     * Creates a new RecentChangesWebSocketClient.
     *
     * @param ws The websocket used for data transfer.
     * @param patrolManager The parent recent changes patrol manager.
     */
    constructor(ws : WebSocket, patrolManager : RecentChangesPatrolManager) {
        this.ws = ws;
        this.patrolManager = patrolManager;

        /* When we receive a message, */
        this.ws.addEventListener("message", (messageEvent) => {
            this.wsConnectionMessage(messageEvent);
        });

        /* When the connection is closed unexpectedly, */
        this.ws.addEventListener("close", () => {
            RedWarnUI.showSnackbar(
                "The RedWarn connection was closed.",
                { variant: "error" }
            );
        });

        /* When the socket opens, */
        this.ws.addEventListener("open", () => {
            console.log("Opened websocket connection.");
            console.log("Sending handshake...");

            // Send a handshake to ensure compatibility between the RWB and
            // the client.
            ws.send(WSMS.serializeClient({
                direction: Direction.ClientToServer,
                type: "handshake",
                clientVersion: RW_FRONTEND_VERSION,
                clientIdentifier: RW_FRONTEND_IDENTIFIER,
                clientCommitHash: RW_GIT_HASH
            }));
        });
    }

    /** The message matcher used to handle messages to the WebSocket. **/
    messageMatcher = messageMatcherServerToClient<void>({
        handshake: (message) => {
            console.log(`Connected to RedWarn Backend v${message.serverIdentifier}`);
            RedWarnUI.showSnackbar(
                "Established connection with RedWarn.",
                { variant: "success" }
            );
        },

        handshakedone: (message) => {
            if (message.warnings.length !== 0) {
                console.warn("Handshake warnings caught.");
                console.dir(message.warnings);
            }

            this.ws.send(WSMS.serializeClient({
                direction: Direction.ClientToServer,
                type: "wikiswitch",
                wiki: SettingsManager.settings.activeWikis
            }));
        },

        error: (message) => {
            console.error("Websocket experienced error.");
            if (message.fatal) {
                console.error("RCWebSocket error was fatal. Won't continue without user intervention.");
                // TODO Show popup to user.
            } else {
                console.error(message.reason);
                console.dir(message.extra);
                RedWarnUI.showSnackbar(
                    "The RedWarn connection reported an error.",
                    { variant: "error" }
                );
            }
        },

        recentchange: (message) => {
            this.patrolManager.addEntry(message.change);
        }
    });

    /**
     * This function is callled when the WS gets a message. Since we can assume
     * that the message is always serialized, we'll just directly serialize it.
     * @param messageEvent The WS MessageEvent.
     */
    wsConnectionMessage(messageEvent : MessageEvent) : void {
        this.messageMatcher(WSMS.deserializeClient(messageEvent.data));
    }

}