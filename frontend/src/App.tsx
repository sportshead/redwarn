import * as React from "react";
import * as ReactDOM from "react-dom";
import UILoginWindow from "./ui/auth/UILoginWindow";
import AuthorizationManager from "./auth/AuthorizationManager";
import {ThemeProvider} from "@material-ui/core/styles";
import Theme, {DefaultTheme, getTheme} from "./ui/Themes";
import UIPatrolWindow from "./ui/patrol/UIPatrolWindow";
import { RW_FRONTEND_SPLASH_DELAY } from "../../common/src/Constants"; // constants
import SettingsManager from "./settings/SettingsManager";
import { SnackbarProvider } from "notistack";
import SnackbarHandler from "./ui/components/SnackbarHandler";
import RedWarnUI from "./ui/RedWarnUI";

// Interfaces

/**
 *  The properties interface used by the {@link RedWarn} component.
 **/
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IRWProps {}

/**
 * The state interface used by the {@link RedWarn} component.
 **/
interface IRWState {

    theme : Theme;
    activeWindow : JSX.Element;

}

/**
 * The properties interface used by RedWarn windows. This allows windows the opportunity to
 * grab properties from the main RedWarn "app".
 **/
export interface IRWWindowProps {

    className? : string;

}

/**
 * The main RedWarn class. This class represents the entire RedWarn app as a
 * whole, and manages renders to the browser screen.
 **/
export default class RedWarn extends React.Component<IRWProps, IRWState> {

    static rw : RedWarn;

    private dead : boolean;

    static setRedWarnGlobal(rw : RedWarn) : void {
        RedWarn.rw = rw;
    }

    constructor(props : IRWProps, context : unknown) {
        super(props, context);

        // There can only be one RedWarn.
        if (RedWarn.rw != null && !RedWarn.rw.dead) {
            console.error("Two RedWarns?!?! That's not allowed!");
            this.dead = true;
        } else if (RedWarn.rw != null) {
            console.error("Old but dead RedWarn instance detected. That instance will be replaced.");
            RedWarn.setRedWarnGlobal(this);
        } else {
            RedWarn.setRedWarnGlobal(this);
        }

        // Set the state.
        this.state = {
            theme: DefaultTheme,
            activeWindow: this.determineFirstWindow()
        };
    }

    /**
     * Determines the first window that RedWarn will show the user. If the user is already
     * authorized (a.k.a. they have logged in with their Wikipedia account), then they should
     * be automatically forwarded to the patrol screen.
     *
     * @returns The first window to show the user (a JSX Element).
     **/
    determineFirstWindow() : JSX.Element {
        const authorizationManager = new AuthorizationManager();
        if (!authorizationManager.isAuthorized())
            return <UILoginWindow />;
        else
            return <UIPatrolWindow />;
    }

    // Application components.

    /**
     * Renders the current active element.
     **/
    public render() : JSX.Element {
        // Planned actions:
        // 1. [ ] Load/generate user settings
        // 2. [ ] Download required cached materials
        //    a. [ ] Download misc. cached materials
        //    b. [X] Inject MUI theme into DOM.
        // 3. [X] Setup interface
        // 4. [X] Render!
        //
        // Loading screen will be removed after render (see below).

        // Inject the MUI theme into DOM.
        RedWarnUI.injectTheme();

        // Show RedWarn!
        return <ThemeProvider theme={getTheme(this.state.theme)}>
            <SnackbarProvider maxSnack={3}>
                <SnackbarHandler/>
                {this.state.activeWindow}
            </SnackbarProvider>
        </ThemeProvider>;
    }

}

// Actual rendering bit (epic swag)
(async () => {
    if (!location.protocol.includes("http")) {
        throw Error("Protocol must be http(s)! Aborting.");
    }

    // Check for settings here.
    await SettingsManager.loadSettings();

    ReactDOM.render(
        <RedWarn />,
        document.getElementById("app")
    );

    document.getElementById("loading")
        .addEventListener("transitionend", function () {
            this.parentElement.removeChild(this);
        });

    // Show the splash screen for a set amount of time.
    setTimeout(() => {
        document.getElementById("loading").classList.remove("active");
    }, RW_FRONTEND_SPLASH_DELAY);
})();
