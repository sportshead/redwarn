import ExpandedMWRecentChange from "../../../common/src/objects/ExpandedMWRecentChange";
import RecentChangesWebSocketClient from "../ws/RecentChangesWebSocketClient";

export default class RecentChangesPatrolManager {

    /** The WebSocket client **/
    recentChangeSocketClient : RecentChangesWebSocketClient;
    /** The recent change entries **/
    entries : ExpandedMWRecentChange[] = [];

    private entryAddedCallbacks : ((entry : ExpandedMWRecentChange) => void)[] = [];
    private entryRemovedCallbacks : ((entry : ExpandedMWRecentChange) => void)[] = [];

    constructor() {
        const websocketURL =
            `${window.location.protocol === "https:" ? "wss" : "ws"}://${window.location.host}/ws/recent_changes`;
        this.recentChangeSocketClient = new RecentChangesWebSocketClient(
            new WebSocket(websocketURL),
            this
        );
    }

    enumerate() : ExpandedMWRecentChange[] {
        return this.entries;
    }

    addEntry(entry : ExpandedMWRecentChange) : void {
        this.entries.splice(0, 0, entry);
        this.entryAddedCallbacks.forEach(v => {
            v(entry);
        });
    }

    removeEntry(entry : ExpandedMWRecentChange) : void {
        this.entries.filter(e => e !== entry);
        this.entryRemovedCallbacks.forEach(v => {
            v(entry);
        });
    }

    registerCallback(
        action : "add" | "remove",
        callback : ((entry : ExpandedMWRecentChange) => void)) : void {
        switch(action) {
            case "add":
                this.entryAddedCallbacks.push(callback);
                break;
            case "remove":
                this.entryRemovedCallbacks.push(callback);
                break;
        }
    }

    // Leijurv, save me from this hell.

    deregisterCallback(
        action : "add" | "remove",
        callback : ((entry : ExpandedMWRecentChange) => void)) : void {
        switch(action) {
            case "add":
                this.entryAddedCallbacks = this.entryAddedCallbacks.filter(c => c !== callback);
                break;
            case "remove":
                this.entryRemovedCallbacks = this.entryRemovedCallbacks.filter(c => c !== callback);
                break;
        }
    }

}