import * as React from "react";
import {RedWarnWordmark, RedWarnWordmarkDark, RedWarnWordmarkWhite} from "../../Resources";
import useTheme from "@material-ui/core/styles/useTheme";

/**
 * An image element containing the RedWarn wordmark.
 **/
export function RWWordmark() : JSX.Element {
    return <img className={"rw-logo"} src={RedWarnWordmark} alt={"RedWarn logo"}/>;
}

/**
 * An image element containing the RedWarn wordmark (for dark backgrounds).
 **/
export function RWWordmarkDark() : JSX.Element {
    return <img className={"rw-logo"} src={RedWarnWordmarkDark} alt={"RedWarn logo"}/>;
}

/**
 * An image element containing the RedWarn wordmark (for dark backgrounds).
 **/
export function RWWordmarkWhite() : JSX.Element {
    return <img className={"rw-logo"} src={RedWarnWordmarkWhite} alt={"RedWarn logo"}/>;
}

/**
 * An image element containing the RedWarn wordmark (switches depending on theme type).
 **/
export function RWWordmarkAuto() : JSX.Element {
    const classes = useTheme();
    if (classes.palette.type === "dark")
        return RWWordmark();
    else
        return RWWordmarkDark();
}

/**
 * An image element containing the RedWarn wordmark (switches depending on theme type).
 *
 * Haven't actually tested this wordmark yet.
 **/
export function RWWordmarkPrimaryContrasted() : JSX.Element {
    const classes = useTheme();

    /* Parse color to RGB */
    const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    const color = classes.palette.primary.main.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);

    /* Check luminance and return respective logo. */
    if (result) {
        return ((0.2126 * +(parseInt(result[1], 16)))
            + (0.7152 * +(parseInt(result[2], 16)))
            + (0.0722 * +(parseInt(result[3], 16)))) > 128 ?
            RWWordmark() : RWWordmarkWhite();
    } else
        return RWWordmark();
}