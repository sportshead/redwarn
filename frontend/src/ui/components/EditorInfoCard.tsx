// WARNING: COMPONENT NOT DONE YET.
// Some backend parts still have to be done. Commenting this entire
// file out for now so I don't throw off the compiler.

// import React = require("react");
// import {Card, CardContent, Typography} from "@material-ui/core";
// import UserData from "../../../../common/src/objects/UserData";
//
// export default class EditorInfoCard extends React.Component<
//     {editor : UserData }, any
// > {
//
//     // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
//     constructor(props : {editor : UserData}, state: any) {
//         super(props, state);
//     }
//
//     render() : JSX.Element {
//         const editor = this.props.editor;
//
//         return <Card
//             id={`editorinfo-${editor.name}`}
//             className={"rw-popup-editorinfo"}>
//             {
//                 editor.populated ? <CardContent>
//                     <Typography
//                         className={"rw-popup-editorinfo-editcount"}
//                         variant={"caption"}
//                         component={"p"}
//                         gutterBottom>
//                         {editor.editCount.toLocaleString()} {Math.abs(editor.editCount) === 1 ? "edit" : "edits"}
//                     </Typography>
//                     <Typography variant={"h5"} component={"p"}>
//                         {editor.username}
//                     </Typography>
//                 </CardContent> : <CardContent>Loading...</CardContent>
//             }
//         </Card>;
//     }
//
// }