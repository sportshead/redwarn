import * as React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import {Avatar, IconButton} from "@material-ui/core";
import Menu from "@material-ui/icons/Menu";
import {RWWordmarkPrimaryContrasted} from "../components/RWLogo";
import Tooltip from "@material-ui/core/Tooltip";
import Lock from "@material-ui/icons/Lock";
import NotificationImportant from "@material-ui/icons/NotificationImportant";
import FindInPage from "@material-ui/icons/FindInPage";
import WatchLater from "@material-ui/icons/WatchLater";
import SupervisedUserCircle from "@material-ui/icons/SupervisedUserCircle";
import Cancel from "@material-ui/icons/Cancel";
import Typography from "@material-ui/core/Typography";
import {UIPatrolContext} from "./Contexts";
import Info from "@material-ui/icons/Info";


/**
 * The header for the {@link UIPatrolWindow}. This includes the RW logo, and the user account menu.
 **/
export class UIPatrolHeader extends React.Component<{hotdog? : boolean}> {

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => <AppBar position="static" className={"rw-patrol-header"}>
                    <Toolbar
                        disableGutters={true}
                        className={"rw-patrol-header-toolbar"}
                        color="primary">
                        <div className={"rw-patrol-header-left"}>
                            <IconButton
                                onClick={() => {
                                    context.patrolWindow.setRecentChangesDrawerState();
                                }}
                                color="inherit">
                                {
                                    this.props.hotdog == true ?
                                        <img src={"images/hotdog.svg"} alt={"Hot Dog"}/> : <Menu />
                                }
                            </IconButton>
                            <RWWordmarkPrimaryContrasted />
                        </div>
                        <UIPatrolHeaderTools
                            className={"rw-patrol-header-center"}/>
                        <div className={"rw-patrol-header-right"}>
                            <UIPatrolHeaderPageInfo />
                            {
                                context.patrolWindow.state.activeEntry != null ?
                                    <IconButton
                                        onClick={() => {
                                            context.patrolWindow.setExtraPageInfoState();
                                        }}
                                        color="inherit">
                                        <Info />
                                    </IconButton> : undefined
                            }
                            <IconButton color="inherit">
                                <Avatar>RW</Avatar>
                            </IconButton>
                        </div>
                    </Toolbar>
                </AppBar>
            }
        </UIPatrolContext.Consumer>;
    }

}

/**
 * The tools found at the center of the {@link UIPatrolHeader}. If an article is
 * not selected, this component needs to be hidden.
 **/
export class UIPatrolHeaderTools extends React.Component<{className? : string}> {

    render() : JSX.Element {
        return <div className={`rw-patrol-tools ${this.props.className ?? ""}`}>
            <Tooltip
                title={"Manage Page Protection"}>
                <IconButton color="inherit">
                    <Lock />
                </IconButton>
            </Tooltip>
            <Tooltip
                title={"Alert on Change"}>
                <IconButton color="inherit">
                    <NotificationImportant />
                </IconButton>
            </Tooltip>
            <Tooltip
                title={"Inspector"}>
                <IconButton color="inherit">
                    <FindInPage />
                </IconButton>
            </Tooltip>
            <Tooltip
                title={"Latest Revision"}>
                <IconButton color="inherit">
                    <WatchLater />
                </IconButton>
            </Tooltip>
            <Tooltip
                title={"Editor Assistance"}>
                <IconButton color="inherit">
                    <SupervisedUserCircle />
                </IconButton>
            </Tooltip>
            <Tooltip
                title={"Exit Revision"}>
                <UIPatrolContext.Consumer>
                    {
                        context => <IconButton
                            color={"inherit"}
                            onClick={() => {
                                context.patrolWindow.setState({
                                    activeEntry: null
                                });
                            }}>
                            <Cancel />
                        </IconButton>
                    }
                </UIPatrolContext.Consumer>
            </Tooltip>
        </div>;
    }

}

/**
 * The page info found at the right of the {@link UIPatrolHeader}. If an article is
 * not selected, this component needs to be hidden.
 **/
export class UIPatrolHeaderPageInfo extends React.Component<{className? : string}> {

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => <div className={`rw-patrol-pageinfo ${this.props.className ?? ""}`}>
                    <Typography variant="h6">
                        {context.patrolWindow.state.activeEntry != null ?
                            context.patrolWindow.state.activeEntry.title : ""}
                    </Typography>
                    <Typography variant="caption">
                        {context.patrolWindow.state.activeEntry != null ?
                            context.patrolWindow.state.activeEntry.user : ""}
                    </Typography>
                </div>
            }
        </UIPatrolContext.Consumer>;
    }

}