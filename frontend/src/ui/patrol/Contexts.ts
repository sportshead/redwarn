import * as React from "react";
import RecentChangesPatrolManager from "../../patrol/RecentChangesPatrolManager";
import UIPatrolWindow from "./UIPatrolWindow";

/**
 * RedWarn Patrol Window Context interface.
 */
export interface RedWarnPatrolContext {

    patrolWindow : UIPatrolWindow;
    rcPatrolManager : RecentChangesPatrolManager;

}

/**
 * RedWarn Patrol Window Context.
 */
export const UIPatrolContext = React.createContext(({
    patrolWindow: null,
    rcPatrolManager: null
} as RedWarnPatrolContext));