import * as React from "react";
import Paper from "@material-ui/core/Paper";
import {UIPatrolContext} from "./Contexts";
import {Typography, Chip} from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import ExpandedMWRecentChange from "../../../../common/src/objects/ExpandedMWRecentChange";
import {UIPatrolRCEntryBytes} from "./UIPatrolRecentChanges";

/**
 * The review and reversion panel for the {@link UIPatrolBody}.
 **/
export class UIPatrolReviewPanel extends React.Component {

    static contextType = UIPatrolContext;

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(props : any, state : any) {
        super(props, state);
    }

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => <Paper
                    className={"rw-patrol-review"}
                    elevation={context.patrolWindow.state.activeEntry == null ? 0 : 8}
                    square={true}>
                    {
                        // TODO Please add a placeholder here soon.
                        context.patrolWindow.state.activeEntry && [
                            <UIPatrolReviewRevisionPanel key={"revPanel"}/>,
                            <UIPatrolReviewPageInfoPanel key={"infoPanel"} />
                        ]
                    }
                </Paper>
            }
        </UIPatrolContext.Consumer>;
    }
}

/**
 * The extra page information panel for the {@link UIPatrolReviewPanel}.
 **/
export class UIPatrolReviewRevisionPanel extends React.Component {

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(props : any, state : any) {
        super(props, state);
    }

    render() : JSX.Element {
        return <div
            className={"rw-patrol-review-rev"}>
            <UIPatrolReviewRevisionInfo />
            {/* TODO Reversion actions */}
            {/* TODO Diff viewer */}
            {/* TODO Diff preview panel */}
        </div>;
    }
}

export class UIPatrolReviewRevisionInfo extends React.Component {

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => {
                    const activeEntry : ExpandedMWRecentChange
                        = context.patrolWindow.state.activeEntry;

                    return <div className={"rw-patrol-review-rev-pageinfo"}>
                        {/* Page title */}
                        <Typography
                            noWrap={true}
                            className={"rw-patrol-review-rev-pagetitle"}
                            variant={"h4"}>
                            {activeEntry.title}
                        </Typography>

                        {/* Byte difference */}
                        <Typography variant={"caption"}>
                            <UIPatrolRCEntryBytes bytes={activeEntry.bytes} />
                        </Typography>

                        {/* Editing user */}
                        <div className={"rw-patrol-review-rev-editor"}>
                            <Chip
                                avatar={<Avatar>{activeEntry.user[0]}</Avatar>}
                                label={activeEntry.user}
                                onClick={() => {
                                    // Show the editor card.
                                }} />
                        </div>

                        {/* Edit summary */}
                        <Typography
                            className={"rw-patrol-review-rev-editsummary"}
                            variant={"body1"}>
                            {activeEntry.comment}
                        </Typography>

                        {/* TODO Edit Tags */}
                    </div>;
                }
            }
        </UIPatrolContext.Consumer>;
    }

}

/**
 * The extra page information panel for the {@link UIPatrolReviewPanel}.
 **/
export class UIPatrolReviewPageInfoPanel extends React.Component {

    static contextType = UIPatrolContext;

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(props : any, state : any) {
        super(props, state);
    }

    render() : JSX.Element {
        return <UIPatrolContext.Consumer>
            {
                context => <Paper
                    className={"rw-patrol-review-pageinfo" + (
                        context.patrolWindow.state.extraPageInfoDrawerOpen ? " open" : ""
                    )}
                    elevation={10}
                    square>
                    {/* TODO xtools Page info */}
                    {/* TODO User info */}
                    {/* page revision history */}
                </Paper>
            }
        </UIPatrolContext.Consumer>;
    }
}