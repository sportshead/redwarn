import * as React from "react";
import {Drawer} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import DynamicFeed from "@material-ui/icons/DynamicFeed";
import Typography from "@material-ui/core/Typography";
import {UIPatrolContext} from "./Contexts";
import ExpandedMWRecentChange from "../../../../common/src/objects/ExpandedMWRecentChange";

/**
 * The recent changes panel for the {@link UIPatrolBody}.
 **/
export class UIPatrolRCPanel extends React.Component {

    static contextType = UIPatrolContext;

    constructor(props : unknown, state : unknown) {
        super(props, state);

        this.context.rcPatrolManager.registerCallback("add", () => {
            this.setState({});
        });
    }

    render() : JSX.Element {
        const entries = this.context.rcPatrolManager.enumerate().map(
            (entry : ExpandedMWRecentChange) =>
                <UIPatrolRCEntry key={entry.revision.new} patrolEntry={entry}/>
        );
        return <UIPatrolContext.Consumer>
            {
                context => <Drawer
                    PaperProps={{
                        className: "rw-patrol-recentchanges scroll-left"
                    }}
                    className={"rw-patrol-recentchanges-container " + (
                        context.patrolWindow.state.recentChangesDrawerOpen ? "" : " inactive"
                    )}
                    variant="persistent"
                    anchor="left"
                    open={context.patrolWindow.state.recentChangesDrawerOpen}>
                    <div>
                        {entries}
                        <Paper
                            elevation={0}
                            square={true}
                            className={"rw-patrol-recentchanges-end"}
                            onClick={() => {
                                context.patrolWindow.setState({
                                    activeEntry: null
                                });
                            }}>
                            <DynamicFeed/><br/>
                            <Typography variant={"caption"}>
                                It looks like that&apos;s the end.
                            </Typography>
                        </Paper>
                    </div>
                </Drawer>
            }
        </UIPatrolContext.Consumer>;
    }
}

/**
 * An entry for the recent changes panel. This takes in a {@link ExpandedMWRecentChange} for its
 * information.
 **/
export class UIPatrolRCEntry extends React.Component<{
    patrolEntry : ExpandedMWRecentChange
}> {

    render() : JSX.Element {
        const patrolEntry = this.props.patrolEntry;

        return <UIPatrolContext.Consumer>
            {
                context => {
                    const activeEntry = context.patrolWindow.state.activeEntry;

                    return <Paper
                        elevation={
                            activeEntry == null ? 0 :
                                (activeEntry.revision.new === patrolEntry.revision.new
                                    ? 8 : 0)
                        }
                        square={true}
                        className={"rw-patrol-recentchanges-entry"}
                        data-rw-rev-active={
                            activeEntry != null ?
                                (activeEntry.revision.new === patrolEntry.revision.new ?
                                    "true" : "false") : undefined
                        }
                        onClick={() => {
                            context.patrolWindow.setState({
                                activeEntry: patrolEntry
                            });
                        }}>

                        { /* To darken the entry if another entry is active. */ }
                        <div className={"rw-patrol-recentchanges-overlay"} />

                        { /* First row - page title */ }
                        <Typography
                            variant="h6"
                            className={"rw-patrol-recentchanges-pagename"}>
                            <div>{patrolEntry.title}</div>
                        </Typography>

                        { /* Second row - bytes changed and responsible user */ }
                        <div className={"rw-patrol-recentchanges-metainfo"}>
                            <UIPatrolRCEntryBytes bytes={patrolEntry.bytes}/>
                            |
                            <span className={"rw-patrol-recentchanges-editor-username"}>
                                {patrolEntry.user}
                            </span>
                        </div>

                        { /* Third row - User comment */ }
                        <Typography
                            variant="caption"
                            style={{
                                fontStyle: "italic"
                            }}>
                            <div dangerouslySetInnerHTML={{__html: patrolEntry.parsedcomment}}/>
                        </Typography>
                    </Paper>;
                }
            }
        </UIPatrolContext.Consumer>;
    }

}

export class UIPatrolRCEntryBytes extends React.Component<{bytes : number, short? : boolean}, any> {

    render() : JSX.Element {
        const classes = ["rw-patrol-bytes"];

        // Highlight the bytes by color depending on sign.
        // If the number is 0, the element is colored gray.
        if (this.props.bytes < 0)
            classes.push("rw-patrol-bytes-negative");
        else if (this.props.bytes > 0)
            classes.push("rw-patrol-bytes-positive");

        // Makes the element bold if the edit is larger than 1 kB.
        if (Math.abs(this.props.bytes) > 1000)
            classes.push("rw-patrol-bytes-large");

        return <span className={classes.join(" ")}>
            {
                this.props.short === true ? [
                    Math.sign(this.props.bytes) === -1 ? "-" : "+",
                    Math.abs(this.props.bytes).toLocaleString(),
                ].join(" ") : [
                    Math.abs(this.props.bytes).toLocaleString(),
                    Math.abs(this.props.bytes) !== 1 ? "bytes" : "byte",
                    Math.sign(this.props.bytes) === -1 ? "removed" : "added"
                ].join(" ")
            }
        </span>;
    }

}