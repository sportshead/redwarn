import * as React from "react";
import {IRWWindowProps} from "../../App";
import RecentChangesPatrolManager from "../../patrol/RecentChangesPatrolManager";
import {UIPatrolRCPanel} from "./UIPatrolRecentChanges";
import {UIPatrolReviewPanel} from "./UIPatrolReview";
import { UIPatrolHeader } from "./UIPatrolHeader";
import { UIPatrolContext } from "./Contexts";

import "../style/global.css";
import "../style/patrol.css";
import "../style/patrol-review.css";
import "../style/patrol-recentchanges.css";
import ExpandedMWRecentChange from "../../../../common/src/objects/ExpandedMWRecentChange";

/**
 * RedWarn Patrol Window State.
 */
interface IUIPatrolWindowState {

    recentChangesDrawerOpen: boolean;
    extraPageInfoDrawerOpen: boolean;
    activeEntry : ExpandedMWRecentChange;

}

/**
 * The actual "RedWarn" part of "RedWarn", the UIPatrolWindow is where recent changes are patrolled and
 * reverted.
 **/
export default class UIPatrolWindow extends React.Component<IRWWindowProps, IUIPatrolWindowState> {

    rcPatrolManager = new RecentChangesPatrolManager();

    constructor(props : IRWWindowProps, state : IUIPatrolWindowState) {
        super(props, state);

        this.state = {
            recentChangesDrawerOpen: true,
            extraPageInfoDrawerOpen: true,
            activeEntry: null
        };
    }

    render() : JSX.Element {
        return <div className={"rw-patrol"}>
            <UIPatrolContext.Provider value={{
                patrolWindow: this,
                rcPatrolManager: this.rcPatrolManager
            }}>
                <UIPatrolHeader/>
                <UIPatrolBody/>
            </UIPatrolContext.Provider>
        </div>;
    }

    /**
     * Changes the state of the drawer to either opened or not opened.
     **/
    setRecentChangesDrawerState(open? : boolean) : void {
        if (open == null) open = !this.state.recentChangesDrawerOpen;

        this.setState({
            recentChangesDrawerOpen: open
        });
    }

    /**
     * Changes the state of the extra page info sidebar to either opened or not opened.
     **/
    setExtraPageInfoState(open? : boolean) : void {
        if (open == null) open = !this.state.extraPageInfoDrawerOpen;

        this.setState({
            extraPageInfoDrawerOpen: open
        });
    }

}

/**
 * The body for the {@link UIPatrolWindow}. This includes the recent changes panel, article difference panel,
 * article info and user info panel, and other related drawers.
 **/
export class UIPatrolBody extends React.Component {

    static contextType = UIPatrolContext;

    render() : JSX.Element {
        return <div className={"rw-patrol-body"}>
            <UIPatrolRCPanel />
            <UIPatrolReviewPanel />
        </div>;
    }
}