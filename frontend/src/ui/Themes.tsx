import {createMuiTheme, ThemeOptions, Theme as MUITheme} from "@material-ui/core/styles";

export const LightTheme : ThemeOptions = {
    /* haven't done anything yet. so... */
    palette: {
        type: "light",
        primary: {
            light: "#ff5131",
            main: "#d50000",
            dark: "#9b0000",
            contrastText: "#fff"
        },
        secondary: {
            light: "#67daff",
            main: "#01a9f4",
            dark: "#007ac1",
            contrastText: "#fff"
        }
    }
};

export const DarkTheme : ThemeOptions = {
    palette: {
        type: "dark",
        primary: {
            light: "#6d6d6d",
            main: "#424242",
            dark: "#1b1b1b",
            contrastText: "#fff"
        }
    }
};

enum Theme {
    LightTheme,
    DarkTheme
}

export const DefaultTheme = Theme.LightTheme;

export default Theme;

export function getTheme(theme : Theme) : MUITheme {
    switch (theme) {
        case Theme.LightTheme:
            return createMuiTheme(LightTheme);
        case Theme.DarkTheme:
            return createMuiTheme(DarkTheme);
    }
}