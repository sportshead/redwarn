import {Component} from "react";
import {createMuiTheme} from "@material-ui/core/styles";
import {getTheme} from "./Themes";
import RedWarn from "../App";
import {SnackbarHandler} from "./components/SnackbarHandler";
import {OptionsObject} from "notistack";

/**
 * The RedWarn UI controller. Just a container class for a lot of UI features.
 **/
export default class RedWarnUI {

    static snackbarHandler : Component;

    static showSnackbar(text : string, options: OptionsObject) : void {
        (RedWarnUI.snackbarHandler as SnackbarHandler)
            .showSnackbar(text, options);
    }

    /**
     * Grabs all Material UI theme values, and drops them into the CSS as usable
     * CSS variables.
     **/
    static injectTheme() : void {
        const muiTheme = createMuiTheme(getTheme(RedWarn.rw.state.theme));
        const getValues = (prefix : string, object : Record<string, any>, options : {
            numberUnits: "raw" | "ms" | "s" | "px" | "vh" | "vw" | "vmin" | "vmax" | "em" | "rem"
        } = {
            numberUnits: "raw"
        }) : Record<string, any> => {
            let foundValues : Record<string, any> = {};
            for (const [key, value] of Object.entries(object)) {
                if (typeof value === "object") {
                    foundValues = Object.assign(
                        foundValues,
                        getValues(prefix + "-" + key, value, options)
                    );
                } else if (typeof value === "number") {
                    foundValues[prefix + "-" + key] =
                        (options.numberUnits === "raw" ? value : `${value}${options.numberUnits}`);
                } else if (typeof value === "string") {
                    foundValues[prefix + "-" + key] = value;
                }
            }
            return foundValues;
        };
        let styleVariables = "";
        for (const [key, value] of Object.entries(getValues("--rw", muiTheme.palette)))
            styleVariables = styleVariables + `${key}: ${value};`;
        for (const [key, value] of
            Object.entries(getValues("--rw-transition", muiTheme.transitions, {numberUnits: "ms"})))
            styleVariables = styleVariables + `${key}: ${value};`;
        document.getElementById("redwarn-style").innerHTML = `:root{${styleVariables}}`;
    }

}